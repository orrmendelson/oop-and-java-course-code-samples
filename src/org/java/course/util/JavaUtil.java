package org.java.course.util;
/**
 * Java code samples from the course of:
 * Object Oriented Programming in Java using the Cloud.
 * Course by Orr Mendelson and Hanan Gitliz.
 * 
 * This code is free software and is distributed in the hope 
 * that it will be useful, but WITHOUT ANY WARRANTY.
 *  
 * @author Orr Mendelson 
 * @author Hanan Gitliz
 * @since sdk 1.7
 */

public class JavaUtil {

	public static void sleep(int milliseconds) {
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e1) {
		}
	}
}
