package org.java.course.samples.algorithm;

public interface Algorithm {
	public String algoType="undefined"; //automatically it becomes final static
		//since only public-final-static members are allowed in interface
	//public Algorithm() {} //Interface cannot have a c'tor
	public float calculate(int value, int quantity);
	public void setType();
}
