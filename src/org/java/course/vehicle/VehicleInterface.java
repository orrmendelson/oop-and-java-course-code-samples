package org.java.course.vehicle;

public interface VehicleInterface {
	public int getMaxSpeed();
	public String toString();
}
