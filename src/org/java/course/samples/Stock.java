package org.java.course.samples;
/**
 * Java code samples from the course of:
 * Object Oriented Programming in Java using the Cloud.
 * Course by Orr Mendelson and Hanan Gitliz.
 * 
 * This code is free software and is distributed in the hope 
 * that it will be useful, but WITHOUT ANY WARRANTY.
 *  
 * @author Orr Mendelson 
 * @author Hanan Gitliz
 * @since sdk 1.7
 */

import java.util.Calendar;
import java.util.Date;
//import java.util.Timer;

public class Stock {
	public static final String UNDEFINED_STR_VALUE = "value is undefined";
	public static final float UNDEFINED_TRADE_VALUE = -1;
	
	private String symbol;
	private float ask, bid;
	String bidStr;
	private Date date;
	
	/**
	 * c'tor sample - all members are inputs
	 */
	public Stock(String symbol, float ask, float bid, Date date){
		this.symbol = symbol;
		this.ask = ask;
		this.bid = bid;
		this.date = date;
	}
	
	/**
	 * c'tor sample
	 */
	public Stock(String symbol, float ask, float bid){
//		this.symbol = symbol;
//		this.ask = ask;
//		this.bid = bid;
//		this.date = new Date();
		this(symbol, ask, bid, new Date());
	}
	
	/**
	 * Empty c'tor sample
	 */
	public Stock(){
//		symbol = UNDEFINED_STR_VALUE;
//		ask = UNDEFINED_TRADE_VALUE;
//		bid = UNDEFINED_TRADE_VALUE;
//		date = new Date();
		this(UNDEFINED_STR_VALUE, UNDEFINED_TRADE_VALUE,
				UNDEFINED_TRADE_VALUE, new Date());
	}
	
//	public Stock(Stock stock){
//		this(stock.getSymbol(), stock.getAsk(), 
//				stock.getBid(), stock.getDate());
//	}
	/**
	 * Overloading sample - resetBid
	 */
	public void resetBidStr() {
		resetBidStr(0);
		//bidStr = "Bid string is: "+Float.toString(0);
	}
	public void resetBidStr(float num1) {
		resetBidStr(num1,0);
		//bidStr = "Bid string is: "+Float.toString(num1);
	}
	public void resetBidStr(float num1, float num2) {
		bidStr = "Bid string is: "+Float.toString(num1+num2);
	}
	public void resetBidStr(Stock stock) {
		resetBidStr(stock.getBid(),0);
		//bidStr = "Bid string is: "+Float.toString(stock.getBid());
	}

	public void setDateToNow() {
		date = new Date();
	}
	
	public String toString() {
		return ("Stock: "+symbol+": ask="+ask+", bid="+bid+", date="+date);
	}
	
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	
	private float getBid() {
		return bid;
	}

	public static void main(String[] args) throws InterruptedException {
		Stock stock1;
		stock1 = new Stock();
		System.out.println(stock1);
		Thread.sleep(2000);
		stock1.setDateToNow();
		System.out.println(stock1);
		
		Calendar cal = Calendar.getInstance();
		cal.set(2014, Calendar.NOVEMBER, 15);
		Stock stock2 = new Stock("PIH",	13.1f, 12.4f, 
				cal.getTime());
		System.out.println(stock2);
		
		String str1 = new String("Bananaa");
		String str2 = new String("Bananab");
		if (str1.equalsIgnoreCase(str2)) {
			System.out.println("SAME SAME");
		}
		
		
	}
}
