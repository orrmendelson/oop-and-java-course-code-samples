package org.java.course.samples;
/**
 * Java code samples from the course of:
 * Object Oriented Programming in Java using the Cloud.
 * Course by Orr Mendelson and Hanan Gitliz.
 * 
 * This code is free software and is distributed in the hope 
 * that it will be useful, but WITHOUT ANY WARRANTY.
 *  
 * @author Orr Mendelson 
 * @author Hanan Gitliz
 * @since sdk 1.7
 */

public class Bicycle {

	//static members
	private final int WHEELS_NUMBER;

	// Data Member
	private String ownerName;
	private Brand brand;

	//Constructor: Initializes the data member
	public Bicycle( ) { //empty C'tor
		WHEELS_NUMBER = 2;
		ownerName = "Unknown";
	}
	
	public Bicycle(String name) { // C'tor
		WHEELS_NUMBER = 2;
		ownerName = name;
	}
	 
	//Getter (accessor) Returns the name of this bicycle's owner
	public String getOwner( ) {  //"get"+(member name in camelCase)
		return ownerName;
	}

	//Setter (mutator) Assigns the name of this bicycle's owner
	public void setOwnerName(String name) { //"set"+(member name in camelCase)
		ownerName = name;
	}

	public Brand getBrand() {
		return brand;
	}
	/**
	 * sets the brand of the bicycle
	 * @param brandName
	 */
	public void setBrand(String brandName) {
		this.brand = new Brand(brandName);
		System.out.println("this.brand address: "+this.brand);
	}

	public class Brand {
		private String brandName;
		public Brand(String brandName)  {
			this.brandName = brandName;
		}
	}


 	public static void main(String[] args) {
       	
       	Bicycle bike1, bike2;
       	String owner1, owner2;
       	
       	bike1 = new Bicycle( );
       	bike1.setOwnerName("Adam Smith");
       	bike1.setBrand("Fasta");
 
       	bike2 = bike1;
       	bike2.setOwnerName("Ben Jones");
       	bike2.setBrand("Shina");
       	
       	System.out.println("Bike 2 owner: "+bike2.getOwner());
       	System.out.println("Bike 2 owner: "+bike2.getBrand());

 	}
}