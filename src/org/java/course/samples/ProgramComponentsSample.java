package org.java.course.samples;
/**
 * Java code samples from the course of:
 * Object Oriented Programming in Java using the Cloud.
 * Course by Orr Mendelson and Hanan Gitliz.
 * 
 * This code is free software and is distributed in the hope 
 * that it will be useful, but WITHOUT ANY WARRANTY.
 *  
 * @author Orr Mendelson 
 * @author Hanan Gitliz
 * @since sdk 1.7
 */

//import org.java.course.samples.HelloWorld; 
//(LINE COMMENT)
//import org.java.course.samples.*;  //Takes more time in compilation time

/**
* This class describes program components
* @author Orr & Hanan
*/
public class ProgramComponentsSample {
	private String artistName;
	private String albumTitle;
	private Song[] songsList;

	public ProgramComponentsSample(String albumTitle) {
		this.albumTitle = albumTitle;
	}
	
	public String getArtistName() {
		return artistName;
	}
	
	public void setArtistName() {
		setArtistName("unknown artist","");
	}
	
	public void setArtistName(String name) {
		setArtistName(name,"");
	}
	
	public void setArtistName(String first,String family) {
		this.artistName = first;
		if (family!=null && family != "") {
			this.artistName += " "+family;
		}
	}
	
	/**
	 * This method calculates album total duration
	 * @return album duration in seconds
	 */
	public int getAlbumDuration() {
		int albumDuration = 0;
		
		for (int i = 0; i < songsList.length; i++) { //use CTRL-SPACE
			albumDuration += songsList[i].duration;
		}
		
		return albumDuration;
	}
	
	/**
	 * set song's youtube url of a given song  
	 * @param songName 
	 * @param youtubeURL - url of youtube song in String
	 * @return success (in finding song in album and set its youtube)
	 */
	public boolean setSongYoutubeURL(String songName, String youtubeURL) {
		for (Song song : songsList) {
			if (song.name.equals(songName)) {
				song.setYoutubeURL(youtubeURL);
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * This class represents a single song 
	 * @author Orr & Hanan
	 */
	private class Song {
		private String name;
		private int duration; //in seconds
		private java.lang.String youtubeURL; // you can also use fully qualified name (full package path)
			//however, String is automatically imported since it's in java.lang package
		
		private void setYoutubeURL(String youtubeURL) {
			this.youtubeURL = youtubeURL;
		}
	}
}



