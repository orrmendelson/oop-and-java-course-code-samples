package org.java.course.samples;

public class EnumDemo {
	
	public enum Gender {MALE, FEMALE}
	
	public enum Animal {
		  CAT("Cradle"), DOG("Kennel"), 
		  	FLY("Garbage",Gender.FEMALE);
		  private String residence;
		  private Gender gender;
		  private Animal(String residence){ this.residence=residence; }
		  private Animal(String residence, Gender gender){ 
			  this.residence=residence; this.gender =  gender;
		  }
		  //private c�tors -> no one can add new Animal!
		  public String getResidence() { return residence; }
		  public Gender getGender() { return gender; }
	}

	public static void main(String[] args) {
		Animal cat = Animal.CAT;
		System.out.println("CAT \n Residence: " + cat.getResidence()
				+" , Gender:" + cat.getGender());
		Animal fly = Animal.FLY;
		System.out.println("FLY \n Residence: " + fly.getResidence()+
							" , Gender:" + fly.getGender());
		
	}

}
