package org.java.course.vacumcleaner;

public interface SofisticatedVacumCleaner {
	public final static String origin = "China"; 
	public void suckCarpet();
	public void turnOn();
	public void suckSilently();
}
