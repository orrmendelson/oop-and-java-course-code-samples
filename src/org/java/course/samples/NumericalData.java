package org.java.course.samples;
/**
 * Java code samples from the course of:
 * Object Oriented Programming in Java using the Cloud.
 * Course by Orr Mendelson and Hanan Gitliz.
 * 
 * This code is free software and is distributed in the hope 
 * that it will be useful, but WITHOUT ANY WARRANTY.
 *  
 * @author Orr Mendelson 
 * @author Hanan Gitliz
 * @since sdk 1.7
 */

//import java.math.BigDecimal;

public class NumericalData {
	
	// integer data types:
	private int i1, i2;
	int  i3,i4 = 9;
	int i5 = 4 + 5;
	
	short short1;
	long long1 = 5;
	byte byte1 = 127;
	//byte byte2 = 128; //overflow, in java compilation error.
	byte byte4 = -128;
	//byte byte5 = -129; //overflow, in java compilation error.
	
	// real data types:
	float f1 = 5;
	float f2 = 6f, f3 = 6/3.5f;
	double d1 = 7, d2 = 4.4564456;
	
	private int counterValue = 0;
	
	public static void main(String[] args) {
		
		System.out.println("5/3="+5/3);
		System.out.println("5.0/3="+5.0/3);

		NumericalData nd = new NumericalData();
		nd.i1 = 4/5;
		
		System.out.println("Integers:");
		System.out.println("---------");
		System.out.println("i1=" + nd.i1); //cast to int --> round down (by Math.floor()) 
		System.out.println("i2=" + nd.i2);  
		System.out.println("short1=" + nd.short1);
		System.out.println("long1=" + nd.long1);
		System.out.println();
		
		System.out.println("\nReal:");
		System.out.println("-----");
		System.out.println("f1=" + nd.f1);
		System.out.println("f2=" + nd.f2);
		System.out.println("f3=" + nd.f3);
		System.out.println("d1=" + nd.d1);
		System.out.println("d2=" + nd.d2);

		System.out.println("\nDivisions:");
		System.out.println("---------");
		System.out.println("1. f1/f2=" + (nd.f1/nd.f2) );
		System.out.println("2. f1/f2=" + (nd.f1/0) );
		System.out.println("3. f1/f2=" + (nd.f1/(int)0) );
		System.out.println("4. f1/f2=" + (int)(nd.f1/0) );
		System.out.println("5. f1/f2=" + (int)(nd.f1/(int)0) );
		//System.out.println("6. f1/f2=" + (nd.i1/0) ); //IEEE754 Standard doesn't cover int at all, only for floating point
		
		//nd.b = 5.06; //compilation error
		nd.i2 = (int)5.06; //aha! casting to int works!
		System.out.println("b=" + nd.i2);  
		System.out.println("!true=" + (!true));
		
		System.out.println("\n explicit type casting:");
		float float10 = 6.666f;
		int int10 = (int)float10/3;
		System.out.println("3.333/3=" + int10);  
		
		//power
		System.out.println("2^3=" + Math.pow(2,3));  
		
		System.out.println("modulu (%)");
		//System.out.println("2%0=" + 2%0); //runtime exception  
		System.out.println("5%1=" + 5%1);  
		System.out.println("9%4=" + 9%4);
		
		//subexpression
		System.out.println("\nSubexpression");
		System.out.println("4 * ((counter()/ 3) + 2) * (4 - 12 % counter()) = "+
				4 * ((nd.counter() / 3) + 2) * (4 - 12 % nd.counter()) );

		System.out.println("\nCasting");
		//casting - explicit
		int cast1 = (int)(5f/3f);
		System.out.println("int cast1 = (int)5/3 -> "+cast1);
	    double double_int = (double)(5/3); // double_int=1.0
		System.out.println("double double_int = (double)(5f/3f); -> "+double_int);
	    double double_float = (double)(5f/3f); // double_float=1.6666666269302368
		System.out.println("double double_float = (double)(5f/3f); -> "+double_float);
	    double double_double = (double)(5d/3d); // double_double=1.6666666666666667
		System.out.println("double double_double = (double)(5d/3d); -> "+double_double);
	
		System.out.println("\nShorthand Assignment Operator");
		int sum = 0;
		System.out.println("sum+5 = " + (sum + 5));
		System.out.println("1. sum = " + sum); //0
		
		sum = sum+5;
		System.out.println("2. sum=" + sum); //5
		
		sum += 5;
		System.out.println("3. sum=" + sum); //10
		
		sum *= 2; //20
		sum /= 10; //2
		sum -= -8; //10
		sum %= 3; //1
		System.out.println("4. sum=" + sum); //1
		
		System.out.println("\nIncrement-Decrement Operators");
		int count = 0;
		System.out.println("1 count = " + count);
		System.out.println("2 count = " + count++); //0
		System.out.println("3 count = " + ++count); //2
		System.out.println("4 count = " + count--); //2
		System.out.println("5 count = " + --count); //0
		
		
	}

	public int counter() {
		return ++counterValue;
	}
		
}



