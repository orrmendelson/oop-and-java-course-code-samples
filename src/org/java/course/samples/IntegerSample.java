package org.java.course.samples;

public class IntegerSample {
	public static void main(String[] args) {
		
		Integer I1 = new Integer(100);
		Integer I2 = new Integer(101);
		System.out.println("Is "+I1+"=="+I2+" ? : "+I1.compareTo(I2));
		System.out.println("Float value of "+I1+" is: "+I1.floatValue());
		System.out.println("int value of "+I1+" is: "+I1.intValue());
		System.out.println("IntegerParseInt(\"22\")="+Integer.parseInt("22"));
		System.out.println("IntegerValueOf(\"333\")="+Integer.valueOf("333"));
		System.out.println("IntegerValueOf(\"444\").floatValue()="+Integer.valueOf("444").floatValue());
		
		
		Float F1 = new Float(10.1);
		Float F2 = Float.parseFloat("10.1");
		System.out.println("Is "+F1+"=="+F2+" ? : "+(F1==F2));
		Double D1 = new Double(10.1);
		System.out.println("Is "+D1+"=="+F1+" ? : "+D1.compareTo(F1.doubleValue()));
		
		Long L1 = new Long(20000000*20000000);
		System.out.println("L1 value is: "+L1);
		
	}
}
