package org.java.course.samples;
/**
 * Java code samples from the course of:
 * Object Oriented Programming in Java using the Cloud.
 * Course by Orr Mendelson and Hanan Gitliz.
 * 
 * This code is free software and is distributed in the hope 
 * that it will be useful, but WITHOUT ANY WARRANTY.
 *  
 * @author Orr Mendelson 
 * @author Hanan Gitliz
 * @since sdk 1.7
 */


//import java.util.Random;

public class PowerAnalyzer {

	enum POWER_TYPES {MATH, ITERATIVE, RECURSIVE};

	public static void main(String[] args) {

		int MIN_BASE = 40;
		int MIN_EXP = 40;
		int DELTA = 10;
		int MAX_SAMPLING_NUM = 10000000;
		

		//warmup JVM
		System.out.println("Warming up JVM... 2 seconds and back...");
		for (int i = 0; i < 10; i++) {			
			double loadDummy = Math.pow(10, Math.round(Math.random()*4));
		}

		PowerAnalyzer power = new PowerAnalyzer();
		for (int sampling_num=1; sampling_num< MAX_SAMPLING_NUM; sampling_num=sampling_num*10) {
			System.out.println("\nAverage power calculation time with "+sampling_num+" samples:");
			power.calculateAverageTaskPeriod(POWER_TYPES.MATH, MIN_BASE, MIN_EXP, DELTA, sampling_num);
			power.calculateAverageTaskPeriod(POWER_TYPES.ITERATIVE, MIN_BASE, MIN_EXP, DELTA, sampling_num);
			power.calculateAverageTaskPeriod(POWER_TYPES.RECURSIVE, MIN_BASE, MIN_EXP, DELTA, sampling_num);
		}
		System.out.println("\n*** END OF EXPERIMENT ***");
	}

	/**
	 * calculateAverageTaskPeriod method
	 * @param type - of power calc
	 * @param min_base
	 * @param min_exp
	 * @param delta
	 * @param samplesNum 
	 */
	public long calculateAverageTaskPeriod(POWER_TYPES type, int min_base, int min_exp, int delta, int samplesNum) {		

		long time, taskPeriod, accumulatedPeriods=0;
		for (int i=0; i<samplesNum; i++) {
			taskPeriod=0;
			int base = min_base + (int)(Math.random()*delta);
			int exp = min_exp + (int)(Math.random()*delta);
			switch (type) {
			case MATH:
				time = System.nanoTime();
				Math.pow(base,exp);
				taskPeriod = (System.nanoTime() - time);
				break;
			case ITERATIVE:
				time = System.nanoTime();
				powerIterative(base,exp);
				taskPeriod = (System.nanoTime() - time);
				break;
			case RECURSIVE:
				time = System.nanoTime();
				powerRecursive(base,exp);
				taskPeriod = (System.nanoTime() - time);
				break;
			}
			accumulatedPeriods += taskPeriod;
		}

		long averagePeriod = accumulatedPeriods/samplesNum;
		System.out.println( averagePeriod + " nanosec. by "+type+". Average of "+samplesNum+" samples.");
		return averagePeriod;
	}             

	/**
	 * powerRecursive
	 * @param base
	 * @param exp
	 * @return
	 */
	public double powerRecursive(int base, int exp) {
		if ( exp > 1 ) {
			return (base*powerRecursive(base, exp-1));
		} 
		if ( exp == 0) { return 1;}
		return base; 
	}	

	/**
	 * powerIterative
	 * @param base
	 * @param exp
	 * @return
	 */
	public double powerIterative(int base, int exp) {
		long result = 1;
		for (int j=0; j<exp; j++) {
			result *= base;
		}
		return result;
	}
}
