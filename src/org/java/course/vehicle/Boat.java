package org.java.course.vehicle;

public class Boat extends WaterVehicle {

	public Boat(int waterSpeed) {
		this.minWaterDepthForMoving = waterSpeed;
	}

	@Override
	public int getMinumWaterDepthForMoving() {
		return minWaterDepthForMoving + 5;
	}
	@Override
	public int getMaxSpeed() {
		return maxSpeed;
	}

	public String toString() {
		return "Boat: min water depth = "+ getMinumWaterDepthForMoving();
	}

	public String getRegulator() {
		return "US Navy";
	}

	public static void main(String[] args) {
		Boat boat = new Boat(100);
		System.out.println("Boat main(): \n" + boat);

	}

	protected void validateStanding() {
		throttleInNeutral();
	}

	protected void igniteEngine() {
		pushIgnitionKnob();
	}

	protected void startAccelerating() {
		pushThrottleForward();
	}

	private void throttleInNeutral() {
		// TODO Auto-generated method stub

	}
	private void pushIgnitionKnob() {
		// TODO Auto-generated method stub

	}
	private void pushThrottleForward() {
		// TODO Auto-generated method stub

	}

}
