package org.java.course.farm;

public interface Animal {
	void walk();
	void showValueToFarm();
}
