package org.java.course.samples;
/**
 * Java code samples from the course of:
 * Object Oriented Programming in Java using the Cloud.
 * Course by Orr Mendelson and Hanan Gitliz.
 * 
 * This code is free software and is distributed in the hope 
 * that it will be useful, but WITHOUT ANY WARRANTY.
 *  
 * @author Hanan Gitliz
 * @author Orr Mendelson 
 * @since sdk 1.7
 */

import org.java.course.samples.exception.AccountAlreadyExistException;
import org.java.course.util.JavaUtil;

public class ExceptionsDemo {
	
	/**
	 * Demo try-catch blocks.
	 */
	public void myTryCatch() {
		try {
			System.out.println("2/0 = " + (2/0));
		} catch(ArithmeticException ae) {
			System.out.println("Error: divide integer by zero!");
		}
	}
	
	/**
	 * Demo multiple catch blocks.
	 */
	public void myMultipleCatchBlock(){
		try {
			double x = 2/0;
			System.out.println("x = " + x);
		} catch(ArithmeticException ae) {
			System.out.println("Error: divide integer by zero!");
		} catch(Exception e) {
			System.out.println("Another error, reason: " + e.getMessage());
		}

		//Unreachable catch block for ArithmeticException. It is already handled by the catch block for Exception
		/*try{
			double x = 2/0;
			System.out.println("x = " + x);
		}catch(Exception e) {
			e.printStackTrace();
		}catch(ArithmeticException ae) {
			System.out.println("Devide by zero");
		}*/
		
		try {
			setSymbol_Thrower("Joe");
		} catch(ArithmeticException ae) {
			System.out.println("Error: divide integer by zero!");
		} catch(Exception e) {
			System.out.println("Another error, reason: " + e.getMessage());
		}
	}
	
	public void myFinallyBlock() {
		try {
//			System.out.println("going to calculate: 2/0");
//			double x = 2/0;
//			System.out.println("x = " + x);
		} catch(ArithmeticException ae) {
			System.out.println("Error: divide integer by zero!");
		} catch(Exception e) {
			System.out.println("Another error, reason: " + e.getMessage());
		} finally {
			System.out.println("In finally block");
		}
		System.out.println("after try-catch-finally");
	}

	/**
	 * Demo exception thrower method.
	 */
	public void setSymbol_Thrower(String symbol) throws Exception{
		System.out.println("start adding stock by symbol: "+symbol+ " - going to throw Exception");
		if (symbol.equals("")) {
			throw new Exception("setSymbol_Thrower - Empty symbol name (a new Exception!)");
		}
	}

	/**
	 * Demo exception propagator method.
	 */
	public void addStockBySymbol_Propagator(String newStockSymbol) throws Exception {
		try {
			setSymbol_Thrower(newStockSymbol);
		} catch(Exception e) {
			System.out.println("addStockBySymbol_Propagator - going to propagate Exception");
			throw e;
		}
	}

	/**
	 * Demo exception catcher method.
	 */
	public void stocksManager_Catcher(String newStockSymbol){
		try {
			addStockBySymbol_Propagator(newStockSymbol);
		} catch(Exception e) {
			System.out.println("stocksManager_Catcher - caught Exception, here is the stack trace:");
			e.printStackTrace();
		}
	}
	
	public void demoProgrammerDefinedException() {
		Account account;
		try {
			account = new Account("Lady Gaga");
			System.out.println("Account of " + account.getName() + " was created successfully");
		} catch (AccountAlreadyExistException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void main(String[] args) {

		ExceptionsDemo exceptionsDemo = new ExceptionsDemo();

		System.out.println("\n  Demo try-catch blocks:");
		System.out.println("-----------------------------");
		exceptionsDemo.myTryCatch();;
		
		System.out.println("\n  Demo multiple catch blocks:");
		System.out.println("-----------------------------");
		exceptionsDemo.myMultipleCatchBlock();

		System.out.println("\n  Demo finally block:");
		System.out.println("---------------------");
		exceptionsDemo.myFinallyBlock();

		System.out.println("\n  Demo thrower, propagator and catcher methods:");
		System.out.println("---------------------------------------------");
		String newStockSymbol = "";
		exceptionsDemo.stocksManager_Catcher(newStockSymbol);

		JavaUtil.sleep(10);
		System.out.println("\nDemo programmer defined exception:");
		System.out.println("------------------------------------");
		exceptionsDemo.demoProgrammerDefinedException();
	}
	
	public class Account {
		private static final String myExistingAccountName = "Lady Gaga";
		
		private final String name;

		public Account(String name) throws AccountAlreadyExistException {
			if(name.equalsIgnoreCase(myExistingAccountName)) {
				throw new AccountAlreadyExistException(myExistingAccountName);
			}
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
	}
}
