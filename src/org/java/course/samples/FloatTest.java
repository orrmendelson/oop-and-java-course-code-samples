package org.java.course.samples;
/**
 * Java code samples from the course of:
 * Object Oriented Programming in Java using the Cloud.
 * Course by Orr Mendelson and Hanan Gitliz.
 * 
 * This code is free software and is distributed in the hope 
 * that it will be useful, but WITHOUT ANY WARRANTY.
 *  
 * @author Orr Mendelson 
 * @author Hanan Gitliz
 * @since sdk 1.7
 */

public class FloatTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// compares the two specified float values
		float f1 = 10.4f;
		float f2 = 10.5f;
		int result = Float.compare(f1, f2);

		if(result > 0) {
			System.out.println("f1 > f2 : "+f1+">"+f2);
		}
		else if(result < 0) {
			System.out.println("f1 < f2 : "+f1+"<"+f2);
		}
		else {
			System.out.println("f1 == f2 : "+f1+"=="+f2);
		}
	}
}

