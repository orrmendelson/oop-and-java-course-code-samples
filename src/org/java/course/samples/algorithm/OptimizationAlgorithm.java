package org.java.course.samples.algorithm;

public abstract class OptimizationAlgorithm implements Algorithm {
	String type;
	
	public OptimizationAlgorithm() {
		setType("Optimization");
	}
	
	protected void setType(String inType) {
		//this.algoType = string; //cannot assign value anymore to this final
		type = inType;
	}
}
