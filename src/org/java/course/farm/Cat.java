package org.java.course.farm;


public class Cat extends FarmFunctionality implements Animal  {
	public void walk(){
		System.out.println("cat walk.");
	}
	
	public String toString() {
		return "Cat instance";
	}
	public static void main(String[] args) {
		Cat cat = new Cat();
		cat.walk();
	}
}
