package org.java.course.vehicle;

public abstract class AbstractVehicle implements VehicleInterface{
	protected int year;
	protected int maxSpeed;

	public int getYear(){
		return year;
	}

	public abstract String getRegulator();

	public void startDriving(){
		validateStanding();
		igniteEngine();
		startAccelerating();
	}
	protected abstract void validateStanding();
	protected abstract void igniteEngine();
	protected abstract void startAccelerating();

}
