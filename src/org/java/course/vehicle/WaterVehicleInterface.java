package org.java.course.vehicle;

public interface WaterVehicleInterface {
	public int getMinumWaterDepthForMoving();
	public int getMaxSpeed();

}
