package org.java.course.samples;

/**
 * Java code samples from the course of:
 * Object Oriented Programming in Java using the Cloud.
 * Course by Orr Mendelson and Hanan Gitliz.
 * 
 * This code is free software and is distributed in the hope 
 * that it will be useful, but WITHOUT ANY WARRANTY.
 *  
 * @author Orr Mendelson 
 * @author Hanan Gitliz
 * @since sdk 1.7
 */

public class StatementsEnum {

	public enum Priority { HIGH, MEDIUM, LOW};
	public enum Animal {
		CAT("Cradle"), DOG("Kennel"), FLY("Garbage");
		
		private String residence;
		
		private Animal(String residence) {
			//it�s a private c�tor -> no one can add new Animal!
			this.residence = residence;
		}
		public String getResidence() {
			return residence;
		}
	}

	private String type;
	Priority priority;
	Animal animal;
	
	public StatementsEnum() {
		this.type = "Sample";		
		this.priority = Priority.HIGH;
		this.animal = Animal.CAT;
	}
	
	public static void main(String[] args) {
		
		int x = 5; 
		if (x == 5) {
			System.out.println("FIVE, man!");
		} else if (x > 0){
			System.out.println("x > 0");
		} else {
			System.out.println("x <= 0");
		}
		
		/* comparing objects */
		String str1 = new String("java");
		String str2 = new String("java");
		
		System.out.println("str1 = str2 using '==': " + (str1 == str2));
		System.out.println("str1 = str2 using 'equals': " + str1.equals(str2));
		System.out.println("str1 = str2 using 'hashcode': " + (str1.hashCode() == str2.hashCode()));
		System.out.println("str1 = str2 using 'equalsIgnoreCase': " + str1.equalsIgnoreCase(str2));
		
		System.out.println("");
		
		/* switch statement with Enum*/
		Animal cat = Animal.CAT;
		Animal dog = Animal.DOG;
		Animal fly = Animal.FLY;
		
		Animal[] farmAnimals = {cat, dog, fly};
		
		System.out.println("\nMy Animals");
		for (int i=0; i<farmAnimals.length; i++) {
			switch (farmAnimals[i]) {
			   case CAT:
				System.out.println("My cat lives in " + farmAnimals[i].getResidence());
				break;
			   case DOG:
				System.out.println("My dog lives in " + farmAnimals[i].getResidence());
				break;
			   default:
				System.out.println("Not in your zoo.");		
				break;
			}
		}
		
		System.out.println("\nMy Clothes Statement");
		String[] clothes = {"Tshirt", "Socks", "Shoe"}; 
		for (int i=0; i<clothes.length; i++) {
			switch (clothes[i]) {
			   case "Tshirt":
				System.out.println("For me summer is " + clothes[i]);
				break;
			   case "Socks":
				System.out.println("My feet are covered with " + clothes[i]);
				break;
			   default:
				System.out.println("I'm a fashionista!");		
				break;
			}
		}
		
		Priority p = Priority.LOW;
		p = Priority.HIGH;
	}
	
	
}
