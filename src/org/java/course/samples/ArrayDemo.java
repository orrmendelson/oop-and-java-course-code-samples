package org.java.course.samples;

import javax.swing.JTable.PrintMode;

public class ArrayDemo {

	double totalRainfall, averageRainfall;
	
	public void doubleArrayDemo() {
		double monthlyRainfall[] = new double[12];
		monthlyRainfall[0] = 1402.3;
		monthlyRainfall[1] = 2261.2;
		monthlyRainfall[2] = 1876.7;
		totalRainfall = monthlyRainfall[0]+monthlyRainfall[1]+monthlyRainfall[2];
		System.out.println("Total rainfall of 1st quarter is: "+ totalRainfall);
	}
		
	public void doubleArrayLoopDemo() {
		double monthlyRainfall[] = new double[12];
		monthlyRainfall[0] = 1402.3;
		monthlyRainfall[1] = 2261.2;
		monthlyRainfall[2] = 1876.7;
		double totalRainfall = 0;
		for (int i=0; i<monthlyRainfall.length; i++){
			totalRainfall+=monthlyRainfall[i];
		}
		System.out.println("Total rainfall (in loop) is: "+ totalRainfall);
	}
		
	public void stringArrayDemo() {
		String[] monthName = new String[12]; //an array of String
		monthName[0] = "January";
		monthName[1] = "February";
		monthName[2] = "March";		
		System.out.println("Total rainfall of "+monthName[0]+" to "
		 +monthName[2]+" is: "+ totalRainfall);
	}

	public void stockArrayDemo() {
		Stock[] portfolioStocks = new Stock[5];
		System.out.println("My first stock before assignment: "+portfolioStocks[0]);
		portfolioStocks[0] = new Stock("AAA", 10.3f, 9.5f);
		System.out.println("My first stock after assignment: "+portfolioStocks[0]);
		portfolioStocks[1] = portfolioStocks[0];
		System.out.println("My second stock: "+portfolioStocks[1]);
		portfolioStocks[0].setSymbol("BBB");
		System.out.println("My second stock after renaming first: "+portfolioStocks[1]);
	}
	
	public void printMyArray(String[] array){
		for (int i=0; i<array.length; i++) {
			System.out.println(i+1+") "+array[i]);
			array[i] = array[i]+" - changed";
		}
	}
	
	public static void main(String[] args) {
		ArrayDemo arrayDemo = new ArrayDemo();
/*
		org.java.course.samples.Bicycle b = new Bicycle(); 
		arrayDemo.doubleArrayDemo();
		arrayDemo.doubleArrayLoopDemo();
		arrayDemo.stringArrayDemo();
		arrayDemo.stockArrayDemo();
		
		String str1 = "AAA";
//		String str2 = new String("AAA");
		if (str1.equals("AAA")) {
			System.out.println("EQUAL!!!");
		} else {
			System.out.println("NOT");
	
	*/	
		String[] testArray = {"Sunday", "Monday", "Tuesday"};
		arrayDemo.printMyArray(testArray);
		arrayDemo.printMyArray(testArray);
		arrayDemo.printMyArray(testArray);
		
	}

}
