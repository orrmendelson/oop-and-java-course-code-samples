package org.java.course.samples.exception;
/**
 * Java code samples from the course of:
 * Object Oriented Programming in Java using the Cloud.
 * Course by Orr Mendelson and Hanan Gitliz.
 * 
 * This code is free software and is distributed in the hope 
 * that it will be useful, but WITHOUT ANY WARRANTY.
 *  
 * @author Hanan Gitliz
 * @author Orr Mendelson 
 * @since sdk 1.7
 */

/**
 * This exception is thrown when user tries to create and existing account.
 * @author hanang
 */
public class AccountAlreadyExistException extends Exception {
	private static final long serialVersionUID = 1L;

	public AccountAlreadyExistException(String username) {
		super("User " + username + " was already created!");
	}
}
