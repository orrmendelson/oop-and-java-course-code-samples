package org.java.course.vehicle;

public abstract class WaterVehicle extends AbstractVehicle
	implements WaterVehicleInterface {
	protected int floatingPower;
	protected int minWaterDepthForMoving;
	public int getFloatingPower() {
		return floatingPower;
	}
}
