package org.java.course.samples.algorithm;

public class PortfolioOptimizationAlgorithm extends OptimizationAlgorithm {

	private float portfolioTotal;
	public PortfolioOptimizationAlgorithm() {
		super();
		setType("PortfolioOptimization");
	}
	public void setType() {}

	@Override
	public float calculate(int value, int quantity) {
		portfolioTotal = value*quantity;
		return portfolioTotal;
	}

		public float getTotal(){
		return portfolioTotal;
	}
	
	public static void main(String[] args) {
		//Algorithm a = new Algorithm(); //cannot instantiate
		//OptimizationAlgorithm o = new OptimizationAlgorithm(); //cannot instantiate
		PortfolioOptimizationAlgorithm p = new PortfolioOptimizationAlgorithm();
		p.calculate(3, 6);
		System.out.println(p.getTotal());

	}

}
