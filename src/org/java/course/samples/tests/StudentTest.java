package org.java.course.samples.tests;
/**
 * Java code samples from the course of:
 * Object Oriented Programming in Java using the Cloud.
 * Course by Orr Mendelson and Hanan Gitliz.
 * 
 * This code is free software and is distributed in the hope 
 * that it will be useful, but WITHOUT ANY WARRANTY.
 *  
 * @author Orr Mendelson 
 * @author Hanan Gitliz
 * @since sdk 1.7
 */

import static org.junit.Assert.*;

import org.java.course.samples.Student;
import org.junit.Test;

public class StudentTest {

	@Test
	public void testEqualsObjectEmpty() {
		Student s1 = new Student();
		Student s2 = new Student();
		
		assertEquals(s1, s2);
		
	}
	
	@Test
	public void testEqualsObjectWithValue() {
		Student s1 = new Student();
		s1.setId(1);
		s1.setName("Joe");
		
		Student s2 = new Student();
		s2.setId(1);
		s2.setName("Joe");
		
		assertEquals(s1, s2);
		
	}
	
	@Test
	public void testEqualsObjectFalse() {
		Student s1 = new Student();
		s1.setId(1);
		s1.setName("Joe");
		
		Student s2 = new Student();
		s2.setId(1);
		s2.setName("Jane");
		
		assertNotEquals(s1, s2);
		
	}

}
