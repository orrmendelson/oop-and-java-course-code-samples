package org.java.course.samples;

public class ShoeStore {

	public Shoe[] shoes = new Shoe[10];
	public void printAllShoes() {
		System.out.println("My shoes:");
		for (int i=0; i<shoes.length; i++){
			if (shoes[i]!=null) {
				System.out.println(shoes[i]);
			}
		}
	}
	
	public static void main(String[] args) {
		ShoeStore store = new ShoeStore(); 
		Shoe s1 = new Shoe("Nike","Red", 40);
		Shoe s2 = new Shoe(s1);
		store.shoes[0] = s1;
		store.shoes[1] = s2;
		store.shoes[1].setFirma("Adidas");
		store.printAllShoes();	
	}
}
