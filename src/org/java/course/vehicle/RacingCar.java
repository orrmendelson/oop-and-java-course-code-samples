package org.java.course.vehicle;

public class RacingCar extends AbstractVehicle {

	public RacingCar(int speed){
		this.maxSpeed = speed;
	}
	@Override
	public int getMaxSpeed() {
		return maxSpeed;
	}

	@Override
	public String getRegulator() {
		return "Japan";
	}
	public String toString() {
		return "Racing Car: max speed = "+ getMaxSpeed();
	}
	protected void validateStanding() { 
		pushBreakPaddle();
	}
	protected void igniteEngine() {
		switchKey();
	}
	protected void startAccelerating() {
		pushGasPaddle();
		releaseBreakPaddle();
	}
	private void pushBreakPaddle() {
	}
	private void switchKey() {
	}	
	private void releaseBreakPaddle() {
	}
	private void pushGasPaddle() {
	}

}
