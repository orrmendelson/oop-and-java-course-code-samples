package org.java.course.vehicle;

public class VehiclesShop {
	
	private AbstractVehicle[] vehicles;
	private int vehiclesNum = 0;
	
	public AbstractVehicle getVehicle(int index) {
		//TODO validate index ok...
		return vehicles[index];
	}
	
	public VehiclesShop(int maxVehiclesQuantity) {
		vehicles = new AbstractVehicle[maxVehiclesQuantity];
	}
	
	public void addVehicle(AbstractVehicle vehicle) {
		vehicles[vehiclesNum] = vehicle;
		vehiclesNum++;
	}
	public static void main(String[] args) {
		VehiclesShop vs = new VehiclesShop(10);
		vs.addVehicle(new Boat(20));
		vs.addVehicle(new RacingCar(280));
		vs.addVehicle(new Boat(33));

		System.out.println("vehiclesShop: \n ************");
		
		AbstractVehicle tempVehicle;
		for (int i=0; i< vs.vehiclesNum; i++) {
			tempVehicle = vs.getVehicle(i);
			if (tempVehicle instanceof Boat) {
				System.out.println(i+1+") "+tempVehicle);
			}
		}
	}
}
