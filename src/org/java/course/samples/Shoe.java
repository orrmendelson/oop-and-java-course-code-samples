package org.java.course.samples;

public class Shoe {

	private String firma;
	private String color;
	private int size;
	
	public Shoe(String firma, String color, int size) {
		this.firma = firma;
		this.color = color;
		this.size = size;
	}
	
	public Shoe(Shoe s1) {
		this.firma = new String(s1.firma);
		this.color = new String(s1.color);
		this.size = s1.size;
	}

	public String toString() {
		return ("Shoe details: "+this.firma+" "+this.size);
	}
	public static void main(String[] args) {
		Shoe myShoe = new Shoe("Nike","Blue",45);
		System.out.println("my shoe: "+myShoe);
		
		Integer myInt = new Integer(101);
		System.out.println("my Int: "+myInt);

	}

	public void setFirma(String newFirma) {
		this.firma = newFirma;
		
	}

}
