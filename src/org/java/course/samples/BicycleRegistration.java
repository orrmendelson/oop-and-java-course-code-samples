package org.java.course.samples;
/**
 * Java code samples from the course of:
 * Object Oriented Programming in Java using the Cloud.
 * Course by Orr Mendelson and Hanan Gitliz.
 * 
 * This code is free software and is distributed in the hope 
 * that it will be useful, but WITHOUT ANY WARRANTY.
 *  
 * @author Orr Mendelson 
 * @author Hanan Gitliz
 * @since sdk 1.7
 */

public class BicycleRegistration {

	public static void main(String[] args) {
		
		Bicycle bike1, bike2;
		String owner1, owner2;
		
		bike1 = new Bicycle( ); //Create and assign values to bike1
		//bike1.ownerName = "David"; //set value to private modifier -> compilation error
		//bike1.setOwnerName(131);
		bike1.setOwnerName("Adam Smith");
		bike2 = new Bicycle( ); //Create and assign values to bike2
		bike2.setOwnerName("Ben Jones");
		
		//Output the information
		owner1 = bike1.getOwner( );
		owner2 = bike2.getOwner( );
		System.out.println(owner1 + " owns a bicycle.");
		System.out.println(owner2 + " also owns a bicycle.");
		
		bike2.setBrand("Mazman");
		Bicycle.Brand brand = bike2.getBrand();
		System.out.println("brand address: "+brand);
		
	}
}