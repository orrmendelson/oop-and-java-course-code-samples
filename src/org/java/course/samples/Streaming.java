package org.java.course.samples;
/**
 * Java code samples from the course of:
 * Object Oriented Programming in Java using the Cloud.
 * Course by Orr Mendelson and Hanan Gitliz.
 * 
 * This code is free software and is distributed in the hope 
 * that it will be useful, but WITHOUT ANY WARRANTY.
 *  
 * @author Orr Mendelson 
 * @author Hanan Gitliz
 * @since sdk 1.7
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Streaming {

	public static void main(String[] args) {

		final String NASDAQ_STOCK_LIST_CSV = 
				"http://www.nasdaq.com/screening/companies-by-name.aspx?letter=0&exchange=nasdaq&render=download";
		final Logger log = Logger.getLogger(Streaming.class.getSimpleName()); 
		InputStream inputStream = null;

		try {

			//URL: Uniform Resource Locator, a pointer to a "resource" on the World Wide Web
			URL url = new URL(NASDAQ_STOCK_LIST_CSV);

			//URLConnection: a communications-link between the application and a URL
			URLConnection connection = url.openConnection();

			//connection will be closed after given time.
			connection.setConnectTimeout(10000);

			//open stream
			inputStream = url.openStream();
			
			//route stream via reader
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			BufferedReader rd = new BufferedReader (inputStreamReader);

			//this is a header line - just drop it.
			rd.readLine();

			String line = "";
			int i=1;
			while ((line = rd.readLine()) != null) { 
				//when reader reached EOF, it assigns null to line.
				String[] split = line.split(","); //split csv to cells.
				String symbol = split[0].trim(); 
				//take first cell and trim white spaces [very important]
				symbol = symbol.replace("\"", ""); // get rid of quote char. 

				String company = split[1].trim(); 
				//take second cell and trim white spaces [very important]
				company = company.replace("\"", ""); // get rid of quote char.
				//print to console
				System.out.println(i++ + ") Company: " + company + ", symbol: " + symbol);
			}   
		} catch (Exception e) {
			log.log(Level.SEVERE, e.getMessage());
		} finally {
			if(inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					log.log(Level.SEVERE, e.getMessage());
				}
			}
		}
	}
}
