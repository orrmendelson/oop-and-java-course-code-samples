package org.java.course.farm;

public class Dog extends FarmFunctionality implements Animal {
	String serial;
	public void walk(){
		System.out.println("dog walk.");
	}
	public void showValueToFarm() {
		System.out.println("Keep sheep safe from wolfs.");
	}

	public String toString() {
		return "Dog instance";
	}
}
