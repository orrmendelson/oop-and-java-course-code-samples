package org.java.course.samples;

import java.util.*;

public class ListDemo {

	public static void main(String[] args) {
		List<String> listA = new ArrayList<String>();
		List<Integer> listB = new LinkedList<Integer>();
		List<Object> listC = new LinkedList<Object>();
	
		listA.add("John1");
		listA.add("John2");
		listA.add("John3");
		listB.add(1);
		listB.add(2);
		listB.add(3);
		listC.add(listA);
		listC.add(listB);
		
		System.out.println("1 list A: size: "+listA.size()+" , [1]="+listA.get(1));
		System.out.println("2 list B: size: "+listB.size()+" , [1]="+listB.get(1));

		listA.remove("John1");
		System.out.println("3 list A: size: "+listA.size()+" , [1]="+listA.get(1));
		
		listA.add("John4");
		System.out.println("4 list A: size: "+listA.size()+" , [1]="+listA.get(1));
		
		//access via Iterator
		System.out.println("\n 5 Access via Iterator \n ***********************");
		Iterator<String> iterator = listA.iterator();
		int i=0;
		while(iterator.hasNext()){
		  String element = (String) iterator.next();
		  System.out.println("listA: element "+ i++ +": "+element);
		}

		i=0;
		//access via for-loop
		System.out.println("\n 6 Access via new for-loop \n ***********************");
		for(Object object : listA) {
		    String element = (String) object;
		    System.out.println("listA: element "+ i++ +": "+element);
		}

		System.out.println("\n 7 Clear list \n ***********************");
		System.out.println("list A: size: "+listA.size());
		listA.clear();
		System.out.println("8 list A: size: "+listA.size());
	}
}