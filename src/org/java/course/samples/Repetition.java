package org.java.course.samples;
/**
 * Java code samples from the course of:
 * Object Oriented Programming in Java using the Cloud.
 * Course by Orr Mendelson and Hanan Gitliz.
 * 
 * This code is free software and is distributed in the hope 
 * that it will be useful, but WITHOUT ANY WARRANTY.
 *  
 * @author Orr Mendelson 
 * @author Hanan Gitliz
 * @since sdk 1.7
 */

public class Repetition {
	
	private static final int EXAMPLE_1_COUNT = 4;
	private static final int EXAMPLE_2_COUNT = 10;
	private static final int values[] = {3, 5, 3, 67, 3, 454, 87, 34, 254, 56, 65 , 56, 2, 8, 6};
	private static final String FAIL_MSG = "Error, max elements count should be ";
	
	/**
	 * While loop example
	 * @param count - elements to calculate average upon. 
	 * @param values - array of values 
	 * @return
	 */
	public float calcAverageUsingWhileLoop(int values[], int count) {
		
		int total = 0;
		int index = 0;
		
		while (index < count) {
			total += values[index++];
		}
		
		return (total / count);
	}
	
	/**
	 * Do while loop example
	 * @param count - elements to calculate average upon. 
	 * @param values - array of values 
	 * @return
	 */
	public float calcAverageUsingDoWhileLoop(int values[], int count) {
		
		int total = 0;
		int index = 0;
		
		do {
			total += values[index++];
		} while (index < count);
		
		return (total / count);
	}
	
	/**
	 * For loop example
	 * @param count - elements to calculate average upon.
	 * @param values - array of values 
	 * @return
	 */
	public float calcAverageUsingForLoop(int values[], int count) {
		
		int total = 0;
		
		for (int i = 0; i < count; i++) {
			total += values[i];
		}
		
		return (total / count);
	}

	public static void main(String[] args) {
		Repetition repetition = new Repetition();
		
		/* while example*/
		System.out.println("While loop example");
		System.out.println("------------------");
		System.out.println("average for first " + EXAMPLE_1_COUNT + " numbers: " + repetition.calcAverageUsingWhileLoop(values, EXAMPLE_1_COUNT));
		System.out.println("average for first " + EXAMPLE_2_COUNT + " numbers: " + repetition.calcAverageUsingWhileLoop(values, EXAMPLE_2_COUNT));
		
		try {
			System.out.println("this next line should fail due to array out of bounds exception...");
			repetition.calcAverageUsingWhileLoop(values, Repetition.values.length+1);
		}catch(ArrayIndexOutOfBoundsException e) {
			System.out.println(FAIL_MSG + e.getMessage());			
		}
		
		/* do while example*/
		System.out.println("\n Do while loop example");
		System.out.println("------------------");
		System.out.println("average for first " + EXAMPLE_1_COUNT + " numbers: " + repetition.calcAverageUsingWhileLoop(values, EXAMPLE_1_COUNT));
		System.out.println("average for first " + EXAMPLE_2_COUNT + " numbers: " + repetition.calcAverageUsingWhileLoop(values, EXAMPLE_2_COUNT));
		
		try {
			System.out.println("this next line should fail due to array out of bounds exception...");
			repetition.calcAverageUsingDoWhileLoop(values, Repetition.values.length+1);
		}catch(ArrayIndexOutOfBoundsException e) {
			System.out.println(FAIL_MSG + e.getMessage());			
		}

		/* for example*/
		System.out.println("\n Do while loop example");
		System.out.println("------------------");
//		System.out.println("average for first " + EXAMPLE_1_COUNT + " numbers: " + repetition.calcAverageUsingForLoop(EXAMPLE_1_COUNT));
		System.out.println("average for first " + EXAMPLE_2_COUNT + " numbers: " + repetition.calcAverageUsingForLoop(values, EXAMPLE_2_COUNT));
		
		try {
			System.out.println("this next line should fail due to array out of bounds exception...");
			repetition.calcAverageUsingForLoop(values, Repetition.values.length+1);
		}catch(ArrayIndexOutOfBoundsException e) {
			System.out.println(FAIL_MSG + e.getMessage());			
		}
	}
}

