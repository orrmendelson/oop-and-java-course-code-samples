package org.java.course.farm;

public class Farm {
	public static void main(String[] args){
		Animal animals[] = new Animal[5];
		Dog d = new Dog();
		Cat c = new Cat();
		animals[1] = d;
		animals[3] = c;
		System.out.println("Animals: "+animals[1]+" "+animals[3]+". ");
		for(int i = 0; i < animals.length; i++){
			System.out.println("inside loop animals["+i+"]="+animals[i]);
			if (animals[i]==null) {
				continue;
			}
			animals[i].walk();
			System.out.println("inside loop #2");
			animals[i].showValueToFarm();
			System.out.println("end of loop i=="+i);
		}
	}
}

