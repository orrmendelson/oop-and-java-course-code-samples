package org.java.course.samples;

public class Student {

	private int id;
	private String name;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object otherObj) {

		if (this == otherObj) {
            return true;
        }
		
		if (otherObj instanceof Student) {
			Student otherStudent = (Student) otherObj;
			
			if(this.name == null) {
				if(this.id == otherStudent.id && otherStudent.name == null) {
					return true;
				}
			}else if(this.id == otherStudent.id && this.name.equals(otherStudent.name)) {
				return true;
			}
		}
		
		return false;
	}
}
