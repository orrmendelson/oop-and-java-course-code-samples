package org.java.course.samples;
/**
 * Java code samples from the course of:
 * Object Oriented Programming in Java using the Cloud.
 * Course by Orr Mendelson and Hanan Gitliz.
 * 
 * This code is free software and is distributed in the hope 
 * that it will be useful, but WITHOUT ANY WARRANTY.
 *  
 * @author Orr Mendelson 
 * @author Hanan Gitliz
 * @since sdk 1.7
 */

import java.io.File;
import java.io.IOException;

public class FileIO {

	public void demoCurrentDir() throws IOException {
		File currentDir = new File(".");
		
		if(!currentDir.isDirectory()) {
			throw new IOException("I'm not a dir!");
		}
		
		System.out.println("My current path is: " + currentDir.getAbsolutePath());
	}
	
	public static void main(String[] args) {
		FileIO fileIO = new FileIO();
		
		try {
			fileIO.demoCurrentDir();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
